// use the built in http module
import http from 'http'
import fs from "fs";
import util from "util";

const PORT = process.env.PORT || 3030




let status
try{
    status = JSON.parse(fs.readFileSync('status.txt', 'utf8'));
} catch (err) {
    console.error('Fout bij het lezen van het bestand:', err);
}

// convert fs.readFile to a promise using node.js 'util' module
const readFilePromise = util.promisify(fs.readFile)
const writeFilePromise = util.promisify(fs.writeFile)

// ow look, it's a callback
// what should the server do for every request? -> send a response
const server = http.createServer((req, res) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, PUT, OPTIONS');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    res.setHeader('Content-Type', 'application/json');

    // OPTIONS-methode afhandelen
    if (req.method === 'OPTIONS') {
        // De OPTIONS-methode wordt gebruikt voor preflight requests in het kader van CORS.
        // Preflight requests zijn verzoekjes die worden gestuurd door een webbrowser als voorbereiding
        // op een cross-origin request (bijvoorbeeld een GET- of PUT-request) naar een server.
        // De browser stuurt eerst een OPTIONS-verzoek om toestemming te vragen aan de server.

        // Hieronder kun je de logica toevoegen om de vereiste headers voor de OPTIONS-response in te stellen.
        // In dit voorbeeld hebben we dezelfde headers ingesteld als voor alle andere responses.

        // Stuur een lege response terug voor de OPTIONS-request.
        res.writeHead(200);
        res.end();
        return;
    }
    // Verwerk de andere HTTP-methodes (GET, PUT, etc.) hier...
    // Je kunt de logica voor het verwerken van verschillende routes en verzoeken implementeren.

    // Voorbeeld van het versturen van een JSON-response
    if (req.method === 'GET') {
        let response;
        // Endpoint "/red"
        if (req.url === '/red') {
            response = { red: status.red };
        }
        // Endpoint "/blue"
        else if (req.url === '/blue') {
            response = { blue: status.blue };
        }
        // Endpoint "/status"
        else if (req.url === '/status') {
            response = status;
        }
        // Onbekend endpoint
        else {
            res.statusCode = 404;
            res.end(JSON.stringify({ error: 'Pagina niet gevonden' }));
            return;
        }
        res.statusCode = 200;
        res.end(JSON.stringify(response));
    }
    if (req.method === 'PUT') {
        if (req.url === '/red') {
            status.red += 1;
            writeFilePromise('./status.txt', JSON.stringify(status), 'utf8')
                .catch(err=>console.log(err));
            res.end(JSON.stringify({red: status.red}));
        }
        // Endpoint "/blue"
        else if (req.url === '/blue') {
            status.blue += 1;
            writeFilePromise('./status.txt', JSON.stringify(status), 'utf8')
                .catch(err=>console.log(err));
            res.end(JSON.stringify({red: status.blue}));
        }

    }
})

// start listening to requests on a certain port number
server.listen(PORT, () => {
    console.log('Listining to requests on http://localhost:3000')
})